const loginTemplate = require('./templates/loginTemplate.js');
const registerTemplate = require('./templates/registerTemplate.js');
const settingsTemplate = require('./templates/settingsTemplate.js');
const searchListTemplate = require('./templates/searchListTemplate.js');

module.exports = {
  loginTemplate,
  registerTemplate,
  settingsTemplate,
  searchListTemplate,
};
