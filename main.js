// 引入模板库
const {
  loginTemplate,
  registerTemplate,
  settingsTemplate,
  searchListTemplate,
  sidebarNavTemplate,
} = require('./index');

// 使用登录模板
const loginOptions = {
  // 设置自定义选项
  onSubmit: 'handleLoginSubmit',
};

const loginHtml = loginTemplate(loginOptions);

// 将生成的HTML添加到UI中
// ...
