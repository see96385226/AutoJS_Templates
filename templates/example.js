const { createFormTemplate } = require('./templates');

function createLogin(options) {
  const defaultOptions = {
    title: '登录',
    usernamePlaceholder: '用户名',
    passwordPlaceholder: '密码',
    submitText: '登录',
    forgotPasswordText: '忘记密码？',
    registerText: '注册',
  };

  const mergedOptions = { ...defaultOptions, ...options };

  return createFormTemplate(mergedOptions);
}

module.exports = {
  createLogin,
};
