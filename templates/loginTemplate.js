module.exports = function loginTemplate(options) {
  // 在这里编写生成登录模板的代码
const templates = require('./templates');

function createLogin(options) {
  const defaultOptions = {
    title: '登录',
    usernamePlaceholder: '用户名',
    passwordPlaceholder: '密码',
    submitText: '登录',
    forgotPasswordText: '忘记密码？',
    registerText: '注册',
  };

  const mergedOptions = { ...defaultOptions, ...options };

  return templates.createFormTemplate(mergedOptions);
}

module.exports = {
  createLogin,
};

};
