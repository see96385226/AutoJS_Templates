// 导入模板文件
const { createFormTemplate } = require('./templates');

function registerTemplate(options) {
  const defaultOptions = {
    title: '注册',
    usernamePlaceholder: '用户名',
    passwordPlaceholder: '密码',
    confirmPasswordPlaceholder: '确认密码',
    emailPlaceholder: '邮箱',
    submitText: '注册',
    loginText: '已有帐号？登录',
  };

  const mergedOptions = { ...defaultOptions, ...options };

  return createFormTemplate(mergedOptions);
}

module.exports = registerTemplate;
