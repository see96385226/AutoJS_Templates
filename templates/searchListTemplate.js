// 搜索列表界面模板
function searchListTemplate(options) {
  const defaults = {
    title: '搜索列表',
    searchPlaceholder: '请输入搜索关键字...',
    onSearch: () => {},
    renderItem: () => {},
  };

  const finalOptions = Object.assign({}, defaults, options);

  return `
    <div class="search-list-template">
      <h2>${finalOptions.title}</h2>
      <input type="text" class="search-input" placeholder="${finalOptions.searchPlaceholder}" oninput="${finalOptions.onSearch}" />
      <ul class="list">
        ${finalOptions.renderItem()}
      </ul>
    </div>
  `;
}

module.exports = searchListTemplate;
