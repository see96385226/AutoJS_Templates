// 导入模板文件
const { createSettingsTemplate } = require('./templates');

function settingsTemplate(options) {
  const defaultOptions = {
    title: '设置',
    sections: [],
  };

  const mergedOptions = { ...defaultOptions, ...options };

  return createSettingsTemplate(mergedOptions);
}

module.exports = settingsTemplate;
